import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import CombosS1 from "../components/combos-s1";
import CombosS2 from "../components/combos-s2";
import CombosS3 from "../components/combos-s3";

class Combos extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <CombosS1/>
                <CombosS2/>
                <CombosS3/>
            </Container>    
        );
    }
}

export default Combos;