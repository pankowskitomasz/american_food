import React,{Component} from "react";
import Container from "../../node_modules/react-bootstrap/Container";
import ReservationS1 from "../components/reservation-s1";

class Reservation extends Component{
    render(){
        return(        
            <Container fluid className="minh-footer-adj p-0">
                <ReservationS1/>
            </Container>    
        );
    }
}

export default Reservation;