import React,{Component} from "react";
import RegisterForm from "../components/register-form";

class Register extends Component{
    render(){
        return(          
            <main className="user-s1 minh-100vh">    
                <RegisterForm backLink={this.props.backLink}/>
            </main>
        );
    }
}

export default Register;